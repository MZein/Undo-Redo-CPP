#include "AppendText.h"

#include <UndoStack.h>

#include <iostream>
#include <memory>

int main() {
	// Create an undo stack with a limit of 250 commands
	mz::UndoStack undo_stack(250);


	// Create an 'original string' for the AppendText command
	std::string origin_string = "Hello world!";

	// Append a string to the original string and create a command for it
	std::string appended_string = " And another hello for you ;)";
	origin_string.append(appended_string);
	
	// Push command to the stack
	undo_stack.BeginMacro("MergeAppendTextCommands");
	AppendText *append = undo_stack.Push<AppendText>(&origin_string, appended_string);

	// Undo and Redo the AppendText command
	undo_stack.Undo();
	undo_stack.Redo();

	// Append another string and create a command for it
	appended_string = " And why not another one: Hello!";
	origin_string.append(appended_string);

	undo_stack.Push<AppendText>(&origin_string, appended_string);
	undo_stack.EndMacro();

	// Undo and redo the merged (first) AppendText command
	undo_stack.Undo();
	undo_stack.Redo();

	// Create a new string, append a new string to it and create a new command for it.
	std::string origin_string_3 = "Another hello world?!";

	appended_string = " Yeah..";
	origin_string_3.append(appended_string);

	// Push the last AppendText command.
	undo_stack.Push<AppendText>(&origin_string_3, appended_string);

	// Undo all commands.
	undo_stack.Undo();
	undo_stack.Undo();

	// Clear the undo stack
	undo_stack.Clear();

	system("pause");
}