#include "AppendText.h"


AppendText::AppendText(std::string *a_origin, const std::string &a_text):
	origin_(a_origin),
	text_(a_text) {

}

AppendText::~AppendText() { /* EMPTY */ }

void AppendText::Undo() {
	// Removes the appended string from the original string.
	*origin_ = origin_->substr(0, origin_->size() - text_.size());
}

void AppendText::Redo() {
	// Appends the appended string to the original string.
	origin_->append(text_);
}

void AppendText::MergeWith(const UndoCommand * a_command) {
	// Merges command if the given command is an AppendText command.
	if (const AppendText* command = dynamic_cast<const AppendText*>(a_command)) {
		if (command->origin_ == origin_) {
			text_ += command->text_;
		}
	}
}
