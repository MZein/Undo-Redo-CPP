/**
* \file AppendText.h
* \author Meine Zeinstra
* \date 19 jun 2018
* \brief AppendText class header.
*
* This file defines the AppendText class and contains all inline functions.
*/
#pragma once

#include <UndoCommand.h>

#include <string>
#include <utility>

/**
*  \brief mz::UndoStack class. Inherits mz::UndoCommand and handles undo and redo when appended text.
*
*  \see mz::UndoCommand
*/
class AppendText final : public mz::UndoCommand {

public:
	/**
	*  \brief Public constructor.
	*
	*  Initializes this command with a pointer to the original string and a string with the text that's added.
	*
	*  \param a_origin A pointer to the original string. The string will change when redo or undo is called. This string has to exist after
	*  \param a_text The string that has been appended to the original string.
	*/
	AppendText(std::string *a_origin, const std::string &a_text);
	/**
	* \brief Empty default destructor.
	*/
	virtual ~AppendText();

	/** 
	*  \brief Undoes the action.
	*
	*  Removes the appended string from the original string.
	*/
	void Undo() override;

	/**
	*  \brief Redoes the action.
	*
	*  Appends the appended string to the original string.
	*/
	void Redo() override;

	/**
	*  \brief Merges given command with this command.
	*
	*  In case the given command is a AppendText command, it appends the appended string of the given command to the appended string of 'this' command.
	*
	*  \param a_command The command that will be merged into 'this' command.
	*/
	void MergeWith(const UndoCommand * a_command) override;

private:
	std::string *origin_; /**< A pointer to the original string. */
	std::string text_; /**< The appended string. */

};
