#include "ChangeFloat.h"

#include <UndoStack.h>

#include <iostream>
#include <memory>

int main() {
	mz::UndoStack undo_stack(250);

	float difference = 2.f;
	float changeable_float_1 = 10.f;

	undo_stack.Push<ChangeFloat>(&changeable_float_1, changeable_float_1 + difference);
	changeable_float_1 += difference;

	undo_stack.BeginMacro("ChangeFloat1");

	difference = 5.f;
	undo_stack.Push<ChangeFloat>(&changeable_float_1, changeable_float_1 + difference);
	changeable_float_1 += difference;

	difference = -12.f;
	undo_stack.Push<ChangeFloat>(&changeable_float_1, changeable_float_1 + difference);
	changeable_float_1 += difference;

	undo_stack.Undo();
	undo_stack.Undo();
	undo_stack.Redo();

	undo_stack.Clear();

	system("pause");

	return 0;
}