/**
* \file ChangeFloat.h
* \author Meine Zeinstra
* \date 19 jun 2018
* \brief ChangeFloat class header.
*
* This file defines the ChangeFloat class and contains all inline functions.
*/
#pragma once

#include <UndoCommand.h>

/**
*  \brief mz::ChangeFloat class. Inherits mz::UndoCommand and handles undo and redo when changing a float.
*
*  \see mz::UndoCommand
*/
class ChangeFloat final : public mz::UndoCommand {

public:
	/**
	*  \brief Public constructor.
	*
	*  Initializes this command with a pointer to the original float and a float with the new value.
	*
	*  \param a_origin A pointer to the original float. The float will change when redo or undo is called. This float must be alive until this command is destroyed.
	*  \param a_new_value The new value of the original float.
	*/
	ChangeFloat(float * a_origin, const float a_new_value);
	/**
	* \brief Empty default destructor.
	*/
	virtual ~ChangeFloat();

	/**
	*  \brief Undoes the action.
	*
	*  Subtracts the difference from the original float.
	*/
	void Undo() override;

	/**
	*  \brief Redoes the action.
	*
	*  Adds the difference to the original float.
	*/
	void Redo() override;

	/**
	*  \brief Merges given command with this command.
	*
	*  In this case, the given command must be a ChangeFloat command, it adds the difference, that is stored in that command, to the difference of 'this'.
	*
	*  \param a_command The command that will be merged into 'this' command.
	*/
	void MergeWith(const UndoCommand * a_command) override;

private:
	float *origin_; /**< A pointer to the original float. */
	float difference_; /**< The difference between the first value of the original float and the new value. */

};
