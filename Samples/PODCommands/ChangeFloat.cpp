#include "ChangeFloat.h"

#include <iostream>


ChangeFloat::ChangeFloat(float * a_origin, const float a_new_value) :
	UndoCommand(),
	origin_(a_origin),
	difference_(a_new_value - *a_origin)
{
	/* EMPTY */
}

ChangeFloat::~ChangeFloat() { /* EMPTY */ }

void ChangeFloat::Undo() {
	std::cout << "Float Change (" << origin_ << ") - Undo: " << *origin_;

	*origin_ -= difference_;

	std::cout << " -> " << *origin_ << std::endl;
}

void ChangeFloat::Redo() {
	std::cout << "Float Change (" << origin_ << ") - Redo: " << *origin_;

	*origin_ += difference_;

	std::cout << " -> " << *origin_ << std::endl;
}

void ChangeFloat::MergeWith(const UndoCommand * a_command) {
	std::cout << "Float Change (" << origin_ << ") - Merging: " << difference_;

	if (const ChangeFloat *change_float_command = dynamic_cast<const ChangeFloat*>(a_command)) {
		if (origin_ == change_float_command->origin_) {
			difference_ += change_float_command->difference_;
		}
	}

	std::cout << " -> " << difference_ << std::endl;
}
