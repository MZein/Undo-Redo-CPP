/**
* \file UndoStack.h
* \author Meine Zeinstra
* \date 13 jun 2018
* \brief mz::UndoStack class header.
*
* This file defines the mz::UndoStack class and contains all inline functions.
*
*/
#pragma once

#include "UndoCommand.h"

#include <vector>
#include <string>

#define BOTTOM_OF_UNDO_STACK -1

/**
*  \brief MZein namespace
*/
namespace mz {

	/**
	*  \brief mz::UndoStack class. Manages the command stack.
	*
	*  \see mz::UndoCommand
	*/
	class UndoStack final {

	public:
		/**
		*  \brief Public default constructor.
		*
		*  \param a_undo_limit An unsigned integer that specifies the size of the undo stack (how many commands can be in the stack).
		*/
		UndoStack(unsigned int a_undo_limit);
		/**
		*  \brief Public default destructor.
		*  
		*  Deletes all commands (with the delete keyword) and clears the stack.
		*/
		~UndoStack();

		/**
		*  \brief Clears the undo stack.
		*  
		*  This function clears the undo stack, but the stack keeps its allocated memory allocated.
		*/
		void Clear();
		/**
		*  \brief Returns the amount of commands.
		*
		*  This function returns an integer that specifies the amount of commands that are currently in the stack.
		*
		*  \return The amount of commands.
		*/
		int Count() const;
		/**
		*  \brief Pushes a specified command to the stack.
		*
		*  This function clears the undo stack, but keeps the undo limit memory allocated. <br>
		*  It removes all available redo's or when the stack is full, it'll remove the command that's at the bottom of the stack and pushes the given command to the top of the stack.<br>
		*  When a macro is active, this function merges the created command with the command that was added first after the macro began.<br>
		*  When the new command is merged, it'll be deleted immediately after mz::UndoCommand::PostPush() is called.
		*
		*  \param a_args The arguments that your object's constructor expect.
		*/
		template<class T, class... Args>
		T* Push(Args&&... a_args);

		/**
		*  \brief Begins a new macro to automaically merge pushed commands.
		*  
		*  This functions begins a new macro (ends the current active macro) if the given macro values are different from the current active macro.
		*  When a new macro began, the first command that is pushed to the stack will be used as 'parent'-command to merge all new commands within this macro with.
		*  
		*  \param a_macro_string The name of the new macro.
		*  \param a_macro_index An extra macro identifier, if the name isn't enough.
		*  
		*  \return true ~ A new macro started. false ~ The same macro identifiers as the current active macro were given.
		*/
		bool BeginMacro(const std::string& a_macro_string, const unsigned int a_macro_index = 0U);

		/**
		*  \brief Ends the current active macro.
		*
		*  This function ends the current active macro.
		*  Call this function when you want to stop merging pushed commands.
		*/
		void EndMacro();

		/**
		*  \brief Undoes the current command.
		*
		*  Calls the Undo function of mz::UndoCommand if the current index is not at the bottom of the stack.<br>
		*  Ends the current active macro.
		*
		*  \see mz::UndoCommand
		*/
		void Undo();
		/**
		*  \brief Redoes the current command.
		*
		*  Calls the Redo function of mz::UndoCommand if the current index in not at the top of the stack.
		*
		*  \see mz::UndoCommand
		*/
		void Redo();

		/**
		*  \brief Sets the undo limit (stack size).
		*
		*  \param a_limit The new undo limit (stack size).
		*/
		void SetUndoLimit(const int a_limit);
		/**
		*  \brief Returns the undo limit (stack size).
		*
		*  \return The undo limit (stack size).
		*/
		int GetUndoLimit() const;

		/**
		*  \brief Checks if the UndoStack can undo.
		*  
		*  \return Whether or not the UndoStack can undo.
		*/
		bool CanUndo() const;

		/**
		*  \brief Checks if the UndoStack can redo.
		*
		*  \return Whether or not the UndoStack can redo.
		*/
		bool CanRedo() const;

		/**
		*  \brief Checks if there's a macro active.
		*
		*  \return Whether or not a macro is active.
		*/
		bool IsMacroActive() const;

	private:
		/**
		*  \brief Deletes all commands from the top of the stack until the given index.
		*
		*  Deletes (with the delete keyword) all commands from the top of the stack until the given index.
		*
		*  \param a_index Deletes all command until this index.
		*/
		void RemoveUntil(int a_index);


		int undo_limit_; /**< Limit of undo commands. */
		int index_; /**< Current index in the stack. */
		std::pair<unsigned int, std::string> current_macro_; /**< The current active macro. */

		bool new_macro_; /**< Identifies if a new macro is created, so it knows that the next new command can be used as main command/parent. */
		mz::UndoCommand *macro_command_; /**< The command that is used to merge new commands with. Is nullptr, when there's no active macro. Is not nullptr, when there's an active macro. */

		std::vector<UndoCommand*> commands_; /**< The stack of commands. */

	};

	inline bool mz::UndoStack::CanUndo() const {
		return index_ > BOTTOM_OF_UNDO_STACK;
	}

	inline bool mz::UndoStack::CanRedo() const {
		return index_ < Count() - 1;
	}

	inline bool mz::UndoStack::IsMacroActive() const {
		return macro_command_ != nullptr;
	}

	inline int UndoStack::Count() const {
		return int(commands_.size());
	};

	template<class T, class ...Args>
	inline T * UndoStack::Push(Args && ...a_args) {
		T *command = new T(a_args...);
#ifdef _DEBUG
		(void) static_cast<UndoCommand*>(command);
#endif
		command->PrePush();

		// Set new command as 'macro command' when a new macro just started.
		if (new_macro_ && command->allow_push_) {
			macro_command_ = command;
		}

		// Return command if it's not allowed to push.
		if (!command->allow_push_) {
			return command;
		}

		// Push command when a new macro just started or when there's no active macro.
		if (new_macro_ || !IsMacroActive()) {
			// Removes all available redo's
			RemoveUntil(index_);

			// Delete command at the bottom of the stack
			// when the stack is full
			if (Count() == undo_limit_) {
				delete *commands_.begin();
				commands_.erase(commands_.begin());
			}

			// Add command to stack
			commands_.push_back(command);
			command->PostPush(UndoCommand::PushType::DEFAULT);

			index_ = Count() - 1;

			new_macro_ = false;

			return command;
		}
		else if (IsMacroActive()) {
			// Merge with 'macro command' when a macro is active.
			macro_command_->MergeWith(command);
			command->PostPush(UndoCommand::PushType::MERGE);
			delete command;
		}

		return nullptr;
	}

};




