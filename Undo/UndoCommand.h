/**
* \file UndoCommand.h
* \author Meine Zeinstra
* \date 13 jun 2018
* \brief mz::UndoCommand class header.
*
* This file defines the mz::UndoCommand class and contains all inline functions.
*/
#pragma once

/**
*  @brief MZein namespace
*/
namespace mz {

	/**
	*  \brief mz::UndoCommand class. The interface of an undo/redo command.
	*  
	*  mz::UndoCommand is an interface for commands to undo and redo actions. 
	*  It must be created with the keyword new. Once added to the undo stack, it will automatically be deleted when it's removed from the stack.
	*  @see mz::UndoStack
	*/
	class UndoCommand {

		friend class UndoStack;

	public:

		/**
		*  \brief Ways how the command can be pushed onto the stack.
		*/
		enum class PushType {
			DEFAULT, /**< The command is pushed onto the stack. */
			MERGE /**< The command is merged with an active 'macro command'. */
		};

		/**
		*  \brief Undoes an action.
		*  
		*  Pure virtual function that must contain code to undo an action.
		*/
		virtual void Undo() = 0;
		/**
		*  \brief Redoes an action.
		*  
		*  Pure virtual function that must contain code to redo an action.
		*/
		virtual void Redo() = 0;

		/**
		*  \brief Merges given command with this command.
		*  
		*  Virtual function that merges the given command with 'this' command. This function doesn't delete the added command.
		*
		*  \param a_command The command that will be merged into 'this' command.
		*/
		virtual void MergeWith(const UndoCommand * a_command) { };

		/**
		*  \brief Event function that is called before pushing.
		*  
		*  Virtual function that can be overriden to add functionality just before any modifications to the stack are performed (before the actual push). 
		*/
		virtual void PrePush() { };
		/**
		*  \brief Event function that is called after pushing.
		*
		*  Virtual function that can be overriden to add functionality just after pushing this command to the stack.
		*
		*  \param a_push_type Specifies how the command is pushed onto the stack.
		*/
		virtual void PostPush(PushType a_push_type) { (void) a_push_type; };

	protected:
		/**
		*  \brief Empty protected default constructor
		*/
		UndoCommand();
		/**
		*  \brief Empty protected default destructor
		*/
		virtual ~UndoCommand();

		/**
		*  \brief Whether or not this command is allowed to push to the stack.
		*  
		*  This boolean can be set in mz::UndoCommand::PrePush() to prevent this command to be push to the stack.<br>
		*  Default value is: true
		*  
		*  <b>INFO #1:</b> When a macro started, the command will not be set as active 'macro command' if it's not allowed to push.<br>
		*  <b>INFO #2:</b> When the command is not allowed to push, it won't merge with the active 'macro command'.<br>
		*  <b>WARNING:</b> You'll have to take care of deleting the command yourself. When a command is not pushed onto the stack, deleting the command is completely your own responsibility.
		*/
		bool allow_push_; 

	};

};
