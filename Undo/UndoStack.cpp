#include "UndoStack.h"

#define EMPTY_MACRO_STRING std::string("")
#define EMPTY_MACRO_INDEX 0
#define EMPTY_COMMAND_MACRO std::make_pair<unsigned int, std::string>(EMPTY_MACRO_INDEX, EMPTY_MACRO_STRING)


mz::UndoStack::UndoStack(unsigned int a_undo_limit) :
	undo_limit_(a_undo_limit),
	index_(BOTTOM_OF_UNDO_STACK),
	current_macro_(EMPTY_COMMAND_MACRO),
	macro_command_(nullptr),
	new_macro_(false) {

	commands_.reserve(undo_limit_);
}

mz::UndoStack::~UndoStack() {
	Clear();
	commands_.clear();
}

void mz::UndoStack::Clear() {
	RemoveUntil(BOTTOM_OF_UNDO_STACK);
}

bool mz::UndoStack::BeginMacro(const std::string& a_macro_string, const unsigned int a_macro_index) {
	if (current_macro_.first != a_macro_index || current_macro_.second != a_macro_string) {
		EndMacro();
		current_macro_.first = a_macro_index;
		current_macro_.second = a_macro_string;
		new_macro_ = true;
		return true;
	}
	return false;
}

void mz::UndoStack::EndMacro() {
	current_macro_ = EMPTY_COMMAND_MACRO;
	macro_command_ = nullptr;
	new_macro_ = false;
}

void mz::UndoStack::Undo() {
	if(CanUndo()) {
		commands_[index_--]->Undo();
		EndMacro();
	}
}

void mz::UndoStack::Redo() {
	if(CanRedo()) {
		commands_[++index_]->Redo();
	}
}

void mz::UndoStack::SetUndoLimit(const int a_limit) {
	undo_limit_ = a_limit;
}

int mz::UndoStack::GetUndoLimit() const {
	return undo_limit_;
}

void mz::UndoStack::RemoveUntil(int a_index) {
	// Set given index to bottom of undo stack if it's less than that.
	if(a_index < BOTTOM_OF_UNDO_STACK) {
		a_index = BOTTOM_OF_UNDO_STACK;
	}

	// Delete all commands until the given index
	while (a_index < Count() - 1) {
		delete commands_[Count() - 1];
		commands_.pop_back();
	}
}
