var classmz_1_1_undo_stack =
[
    [ "UndoStack", "classmz_1_1_undo_stack.html#abb81e11edb316e0535bc14dcbba6b3fd", null ],
    [ "~UndoStack", "classmz_1_1_undo_stack.html#a2de53abcda8e1aef91a73981c1fc6174", null ],
    [ "BeginMacro", "classmz_1_1_undo_stack.html#a568bf7c6a35d46b9ac90eb7b55eb0302", null ],
    [ "CanRedo", "classmz_1_1_undo_stack.html#aa795a0a36b6f90e540686a49a59645c1", null ],
    [ "CanUndo", "classmz_1_1_undo_stack.html#a43bf27c3a099379b8524db0c75a8af19", null ],
    [ "Clear", "classmz_1_1_undo_stack.html#ab76101c6eed041e1e7b9e03e59278929", null ],
    [ "Count", "classmz_1_1_undo_stack.html#ad46845c9e055ed49a7cddbb89e016304", null ],
    [ "EndMacro", "classmz_1_1_undo_stack.html#a08080d55c311b293e07590075645288a", null ],
    [ "GetUndoLimit", "classmz_1_1_undo_stack.html#ab69be64c664c9cc482ac0519255b1654", null ],
    [ "IsMacroActive", "classmz_1_1_undo_stack.html#a5a886fb48118375f1bf829556ee3d60c", null ],
    [ "Push", "classmz_1_1_undo_stack.html#a90d32e7346550f835a888552b0b9755b", null ],
    [ "Redo", "classmz_1_1_undo_stack.html#a8d8bf8c37ece2887a9ccc3e98c3764f8", null ],
    [ "RemoveUntil", "classmz_1_1_undo_stack.html#a82cf9faf713092962fd02a8d7ec4667d", null ],
    [ "SetUndoLimit", "classmz_1_1_undo_stack.html#a9d096f77e9e77b42d3d9bc955109eb68", null ],
    [ "Undo", "classmz_1_1_undo_stack.html#a9be487debd43a87fdf2a4a235e8c4376", null ],
    [ "commands_", "classmz_1_1_undo_stack.html#a006dd22f0d72e58391ee89216089b629", null ],
    [ "current_macro_", "classmz_1_1_undo_stack.html#aa8378c18d919df2343e55884371ae178", null ],
    [ "index_", "classmz_1_1_undo_stack.html#ae78e3f934eca3009303c77cd882ce0d3", null ],
    [ "macro_command_", "classmz_1_1_undo_stack.html#a32268c80e23d748fa15634dcd6180836", null ],
    [ "new_macro_", "classmz_1_1_undo_stack.html#a54234458a5f90c4ef25e53c83c441a1e", null ],
    [ "undo_limit_", "classmz_1_1_undo_stack.html#a89ff7dd6d8736e48d2d4214ada081a36", null ]
];