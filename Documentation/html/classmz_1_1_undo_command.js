var classmz_1_1_undo_command =
[
    [ "PushType", "classmz_1_1_undo_command.html#ac5fc026eb127c5bfb305cfbba793760a", [
      [ "DEFAULT", "classmz_1_1_undo_command.html#ac5fc026eb127c5bfb305cfbba793760aa5b39c8b553c821e7cddc6da64b5bd2ee", null ],
      [ "MERGE", "classmz_1_1_undo_command.html#ac5fc026eb127c5bfb305cfbba793760aa97c51054c5ed46d2f88641ac57ff6347", null ]
    ] ],
    [ "UndoCommand", "classmz_1_1_undo_command.html#acd68bfac4a7ca3adf5321fb682b1be06", null ],
    [ "~UndoCommand", "classmz_1_1_undo_command.html#ac76063d3934b581dffb141ba7b19e3ba", null ],
    [ "MergeWith", "classmz_1_1_undo_command.html#af1f50baceca3f4c9ec5192327147e099", null ],
    [ "PostPush", "classmz_1_1_undo_command.html#abcda0a633c95608226d66256a50f8cc6", null ],
    [ "PrePush", "classmz_1_1_undo_command.html#a0da3167ef4166742591df75a2b4f2a2d", null ],
    [ "Redo", "classmz_1_1_undo_command.html#ac0c06da4677fce2dacef9ef10346c731", null ],
    [ "Undo", "classmz_1_1_undo_command.html#a8e2cca6d518a610aae0beadd28037a1b", null ],
    [ "UndoStack", "classmz_1_1_undo_command.html#aa511a43d61693c080c90e829de561834", null ],
    [ "allow_push_", "classmz_1_1_undo_command.html#a893bff26e0c354ba2dcc2ac7a2efe009", null ]
];